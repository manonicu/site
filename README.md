[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![Build and Deploy](https://github.com/Manonicu/site/actions/workflows/gh-pages.yml/badge.svg)](https://github.com/Manonicu/site/actions/workflows/gh-pages.yml)
![Chakra](https://img.shields.io/badge/chakra-%234ED1C5.svg?style=for-the-badge&logo=chakraui&logoColor=white)
![Next JS](https://img.shields.io/badge/Next-black?style=for-the-badge&logo=next.js&logoColor=white)
![Vercel](https://img.shields.io/badge/vercel-%23000000.svg?style=for-the-badge&logo=vercel&logoColor=white)
![WebStorm](https://img.shields.io/badge/webstorm-143?style=for-the-badge&logo=webstorm&logoColor=white&color=black)

<a href="https://app.daily.dev/Manonicu" style="position:absolute;top:0;right:0"><img src="https://github.com/Manonicu/site/blob/master/devcard.svg" width="200" alt="Manon.icu's Dev Card"/></a>

## Skills Certificates

<a href ="https://www.hackerrank.com/certificates/d88cd5622a28"><img src="https://s2.loli.net/2022/01/14/WOubtrP2GiyBhDe.jpg" width="100"/></a>

## Site links

- https://manon.icu/
- https://site-lk00kjo3z-manonicu.vercel.app/
- https://site-jcqb.pages.dev/
