import CanIUse from 'components/CanIUse'

const components = {
  CanIUse
}

export default components
